import System.IO
import System.Directory
import Data.List
import Data.Maybe
import Control.Monad
import Control.Monad.State

data Command = Command { name :: String
                       , helpText :: String
                       , action :: [String] -> StateT [Todo] IO Bool
                       }

data Status = NotStarted | InProgress | Done deriving (Eq, Show, Ord, Read)

data Todo = Todo { status :: Status
                 , text :: String
                 } deriving (Eq, Show, Read)

getUserInput :: String -> IO String
getUserInput s = do
  hSetBuffering stdout NoBuffering
  putStr s
  hSetBuffering stdout LineBuffering
  getLine

prettyStatus :: Status -> String
prettyStatus NotStarted = "\ESC[31mNTST\ESC[0m"
prettyStatus InProgress = "\ESC[33mINPG\ESC[0m"
prettyStatus Done = "\ESC[32mDONE\ESC[0m"

prettyTodo :: Int -> Todo -> String
prettyTodo i t = unwords
  [ show i
  , prettyStatus . status $ t
  , text t
  ]

addItem :: String -> [Todo] -> [Todo]
addItem s l = Todo { status = NotStarted, text = s } : l

updateAt :: [Todo] -> Int -> [Todo] -> [Todo]
updateAt i n t
  | n > 0 && n <= length t = take (n - 1) t <> i <> drop n t
  | otherwise = t

statusChange :: Status -> Int -> [Todo] -> [Todo]
statusChange s n t = updateAt [(t !! (n - 1)) { status = s }] n t

deleteAt :: Int -> [Todo] -> [Todo]
deleteAt = updateAt []

startItem :: Int -> [Todo] -> [Todo]
startItem = statusChange InProgress

unStartItem :: Int -> [Todo] -> [Todo]
unStartItem = statusChange NotStarted

finishItem :: Int -> [Todo] -> [Todo]
finishItem = statusChange Done

listTodos :: [Todo] -> IO ()
listTodos l = putStr $ unlines (zipWith prettyTodo [1..] l)

printHelp :: IO ()
printHelp = mapM_ (putStrLn . helpText) commands

doActionPrintList :: ([Todo] -> [Todo]) -> StateT [Todo] IO Bool
doActionPrintList action = do
  list <- get
  let newList = action list
  lift (listTodos newList)
  put newList
  return True

parseInput :: String -> (String, [String])
parseInput = fromMaybe ("", []) . uncons . words

handleSort :: [String] -> StateT [Todo] IO Bool
handleSort [] = handleSort ["status"]
handleSort xs
  | "status" `elem` xs = doActionPrintList (sortOn status)
  | "alpha" `elem` xs = doActionPrintList (sortOn text)

commands :: [Command]
commands =
  [ Command { name = "add"
            , helpText = "add s: add new todo item s"
            , action = doActionPrintList . addItem . unwords
            }
  , Command { name = "delete"
            , helpText = "delete n: delete todo item n"
            , action = doActionPrintList . deleteAt . read . head
            }
  , Command { name = "finish"
            , helpText = "finish n: finish todo item n"
            , action = doActionPrintList . finishItem . read . head
            }
  , Command { name = "help"
            , helpText = "help: show this message"
            , action = const (True <$ lift printHelp)
            }
  , Command { name = "list"
            , helpText = "list: list your todo items"
            , action = const $ doActionPrintList id
            }
  , Command { name = "save"
            , helpText = "save: save your changed todo list to disk"
            , action = const (True <$ (writeFile ".todo" <$> gets show))
            }
  , Command { name = "sort"
            , helpText = "sort [type]: sort the todo list"
            , action = handleSort
            }
  , Command { name = "start"
            , helpText = "start n: start todo item n"
            , action = doActionPrintList . startItem . read . head
            }
  , Command { name = "quit"
            , helpText = "exit"
            , action = const $ return False
            }
  , Command { name = "unstart"
            , helpText = "unstart n: unstart todo item n"
            , action = doActionPrintList . unStartItem . read . head
            }
  ]

inputLoop :: StateT [Todo] IO ()
inputLoop = do
  l <- get
  (input, args) <- lift (parseInput <$> getUserInput "> ")
  let command = find ((== input) . name) commands
  continue <- case command of
    Nothing -> True <$ lift (putStrLn ("Unknown command: '" ++ input ++ "'"))
    Just c -> action c args
  when continue inputLoop

main :: IO ()
main = do
  fileExists <- doesFileExist ".todo"
  unless fileExists (writeFile ".todo" "[]")
  fileData <- readFile ".todo"
  void $ runStateT inputLoop (read fileData)
